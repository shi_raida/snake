#include "map.h"
#include <stdexcept>


/****************
 * Constructors *
 ****************/

Map::Map(const std::string& field, unsigned int width, unsigned int height)
        : Map(field, width, height, 0, 0) {}

Map::Map(const std::string& field, unsigned int width, unsigned int height, int min_x, int min_y)
        : m_field(field), m_min_x(min_x), m_min_y(min_y), m_width(width), m_height(height) {}

/***********
 * Getters *
 ***********/

char Map::at(const Position& pos) const {
    if (pos.x < m_min_x || pos.x >= m_min_x + (int)m_width)
        throw std::out_of_range("Map::at() - Position.x is out of range.");
    if (pos.y < m_min_y || pos.y >= m_min_y + (int)m_height)
        throw std::out_of_range("Map::at() - Position.y is out of range.");
    return m_field[(pos.y+m_min_y)*m_width+pos.x+m_min_x];
}

/***********
 * Sprites *
 ***********/

QPixmap* Map::m_wallSprite;
QPixmap* Map::m_waySprite;

void Map::loadSprites() {
    Map::m_wallSprite = new QPixmap("../Snake/sprites/wall.png");
    Map::m_waySprite = new QPixmap("../Snake/sprites/way.png");
}
