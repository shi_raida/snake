#ifndef GAME_H
#define GAME_H

#include "map.h"
#include "snake.h"


class Game
{
public:
    // Constructor
    Game();

    // Getters
    inline bool isGameover() {return m_gameover;}
    inline Map* getMap() {return m_map;}
    inline Snake* getSnake() {return m_snake;}
    inline Position getApple() {return m_apple;}
    inline unsigned int getScore() {return m_score;}

    // Updates
    void move();
    void collisions();

    // Sprites
    static void loadSprites();
    static inline QPixmap getAppleSprite() {return *Game::m_appleSprite;}

private:
    // Sprites
    static QPixmap* m_appleSprite;

    bool m_gameover;
    Map *m_map;
    Snake *m_snake;
    Position m_apple;
    int m_score;
};

#endif // GAME_H
