#ifndef SNAKE_H
#define SNAKE_H

#include <QPixmap>
#include <vector>
#include "position.h"


class Snake
{
public:
    // Constructor
    Snake(std::vector<Position> pos, Position dir);

    // Setters
    void setDir(const Position& dir);

    // Getters
    inline std::vector<Position> getPos() const {return m_pos;}
    inline Position getDir() const {return m_dir;}

    // Updates
    void move();
    void growUp();

    // Sprites
    static void loadSprites();
    static inline QPixmap getHeadTSprite() {return *Snake::m_headTSprite;}
    static inline QPixmap getHeadRSprite() {return *Snake::m_headRSprite;}
    static inline QPixmap getHeadBSprite() {return *Snake::m_headBSprite;}
    static inline QPixmap getHeadLSprite() {return *Snake::m_headLSprite;}
    static inline QPixmap getQueueTSprite() {return *Snake::m_queueTSprite;}
    static inline QPixmap getQueueRSprite() {return *Snake::m_queueRSprite;}
    static inline QPixmap getQueueBSprite() {return *Snake::m_queueBSprite;}
    static inline QPixmap getQueueLSprite() {return *Snake::m_queueLSprite;}
    static inline QPixmap getBodyHSprite() {return *Snake::m_bodyHSprite;}
    static inline QPixmap getBodyVSprite() {return *Snake::m_bodyVSprite;}
    static inline QPixmap getBodyTRSprite() {return *Snake::m_bodyTRSprite;}
    static inline QPixmap getBodyBRSprite() {return *Snake::m_bodyBRSprite;}
    static inline QPixmap getBodyBLSprite() {return *Snake::m_bodyBLSprite;}
    static inline QPixmap getBodyTLSprite() {return *Snake::m_bodyTLSprite;}

private:
    // Sprites
    static QPixmap* m_headTSprite;
    static QPixmap* m_headRSprite;
    static QPixmap* m_headBSprite;
    static QPixmap* m_headLSprite;
    static QPixmap* m_queueTSprite;
    static QPixmap* m_queueRSprite;
    static QPixmap* m_queueBSprite;
    static QPixmap* m_queueLSprite;
    static QPixmap* m_bodyHSprite;
    static QPixmap* m_bodyVSprite;
    static QPixmap* m_bodyTRSprite;
    static QPixmap* m_bodyBRSprite;
    static QPixmap* m_bodyBLSprite;
    static QPixmap* m_bodyTLSprite;

    std::vector<Position> m_pos;
    Position m_dir;
    Position m_nextDir;
    Position m_pop;
};

#endif // SNAKE_H
