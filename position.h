#ifndef POSITION_H
#define POSITION_H


class Position
{
public:
    // Constructor
    Position(int x, int y);

    // Operators
    Position operator+(const Position& p) const;
    Position operator-(const Position& p) const;
    Position operator*(const Position& p) const;
    Position operator/(int c) const;
    bool operator!=(const Position& p) const;
    bool operator==(const Position& p) const;

    // Getter
    double norm() const;

    int x;
    int y;
};

#endif // POSITION_H
