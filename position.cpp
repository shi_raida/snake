#include <cmath>
#include "position.h"

/***************
 * Constructor *
 ***************/

Position::Position(int x, int y) {
    this->x = x;
    this->y = y;
}

/*************
 * Operators *
 *************/

Position Position::operator+(const Position& p) const {
    return Position(x+p.x, y+p.y);
}


Position Position::operator-(const Position& p) const {
    return Position(x-p.x, y-p.y);
}

Position Position::operator*(const Position &p) const {
    return Position(x*p.x, y*p.y);
}

Position Position::operator/(int c) const {
    return Position(x/c, y/c);
}

bool Position::operator!=(const Position& p) const {
    return x != p.x || y != p.y;
}

bool Position::operator==(const Position& p) const {
    return !(*this != p);
}

/***********
 * Getters *
 ***********/

double Position::norm() const {
    return sqrt(pow(x, 2) + pow(y, 2));
}
