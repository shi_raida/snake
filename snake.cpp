#include "snake.h"


/****************
 * Constructors *
 ****************/
Snake::Snake(std::vector<Position> pos, Position dir)
        : m_pos(pos), m_dir(dir), m_nextDir(dir), m_pop(Position(0, 0)) {}

/***********
 * Setters *
 ***********/

void Snake::setDir(const Position& dir) {
    if (dir.norm() != 0) m_nextDir = dir/dir.norm();
    else m_nextDir = dir;
}

/***********
 * Updates *
 ***********/

void Snake::move() {
    m_pop = m_pos[m_pos.size()-1];
    m_pos.pop_back();
    if (m_nextDir*m_dir == Position(0, 0)) m_dir = m_nextDir;
    m_pos.insert(m_pos.begin(), m_pos[0]+m_dir);
}

void Snake::growUp() {
    m_pos.push_back(m_pop);
}

/***********
 * Sprites *
 ***********/

QPixmap* Snake::m_headTSprite;
QPixmap* Snake::m_headRSprite;
QPixmap* Snake::m_headBSprite;
QPixmap* Snake::m_headLSprite;
QPixmap* Snake::m_queueTSprite;
QPixmap* Snake::m_queueRSprite;
QPixmap* Snake::m_queueBSprite;
QPixmap* Snake::m_queueLSprite;
QPixmap* Snake::m_bodyHSprite;
QPixmap* Snake::m_bodyVSprite;
QPixmap* Snake::m_bodyTRSprite;
QPixmap* Snake::m_bodyBRSprite;
QPixmap* Snake::m_bodyBLSprite;
QPixmap* Snake::m_bodyTLSprite;

void Snake::loadSprites() {
    Snake::m_headTSprite = new QPixmap("../Snake/sprites/snake_head_top.png");
    Snake::m_headRSprite = new QPixmap("../Snake/sprites/snake_head_right.png");
    Snake::m_headBSprite = new QPixmap("../Snake/sprites/snake_head_bottom.png");
    Snake::m_headLSprite = new QPixmap("../Snake/sprites/snake_head_left.png");
    Snake::m_queueTSprite = new QPixmap("../Snake/sprites/snake_queue_top.png");
    Snake::m_queueRSprite = new QPixmap("../Snake/sprites/snake_queue_right.png");
    Snake::m_queueBSprite = new QPixmap("../Snake/sprites/snake_queue_bottom.png");
    Snake::m_queueLSprite = new QPixmap("../Snake/sprites/snake_queue_left.png");
    Snake::m_bodyHSprite = new QPixmap("../Snake/sprites/snake_body_horizontal.png");
    Snake::m_bodyVSprite = new QPixmap("../Snake/sprites/snake_body_vertical.png");
    Snake::m_bodyTRSprite = new QPixmap("../Snake/sprites/snake_body_top_right.png");
    Snake::m_bodyBRSprite = new QPixmap("../Snake/sprites/snake_body_bottom_right.png");
    Snake::m_bodyBLSprite = new QPixmap("../Snake/sprites/snake_body_bottom_left.png");
    Snake::m_bodyTLSprite = new QPixmap("../Snake/sprites/snake_body_top_left.png");
}
