#ifndef MAP_H
#define MAP_H

#include <QPixmap>
#include <string>
#include "position.h"

#define WALL '#'
#define WAY '.'


class Map
{
public:
    // Constructors
    Map(const std::string& field, unsigned int width, unsigned int height);
    Map(const std::string& field, unsigned int width, unsigned int height, int min_x, int min_y);

    // Getters
    inline int getMinX() const {return m_min_x;}
    inline int getMaxX() const {return m_min_x+m_width;}
    inline int getMinY() const {return m_min_y;}
    inline int getMaxY() const {return m_min_y+m_height;}
    inline int getWidth() const {return m_width;}
    inline int getHeight() const {return m_height;}
    char at(const Position& pos) const;

    // Sprites
    static void loadSprites();
    static inline QPixmap getWallSprite() {return *Map::m_wallSprite;}
    static inline QPixmap getWaySprite() {return *Map::m_waySprite;}

private:
    static QPixmap* m_wallSprite;
    static QPixmap* m_waySprite;

    std::string m_field;
    int m_min_x;
    int m_min_y;
    unsigned int m_width;
    unsigned int m_height;
};

#endif // MAP_H
