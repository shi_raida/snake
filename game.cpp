#include "game.h"


/***************
 * Constructor *
 ***************/

Game::Game(): m_apple(0, 0) {
    m_gameover = false;

    std::string field = "#################"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#...............#"
                        "#################";
    m_map = new Map(field, 17, 15);

    std::vector<Position> pos;
    pos.push_back(Position(3, 1));
    pos.push_back(Position(2, 1));
    pos.push_back(Position(1, 1));
    m_snake = new Snake(pos, Position(1, 0));

    int x = rand() % (m_map->getWidth()-2) + 1;
    int y = rand() % (m_map->getHeight()-3) + 2;
    m_apple = Position(x, y);

    m_score = 0;
}

/***********
 * Updates *
 ***********/

void Game::move() {
    m_snake->move();
    collisions();
}

void Game::collisions() {
    // Walls
    if (m_map->at(m_snake->getPos()[0]) == WALL) m_gameover = true;

    // Snake
    std::vector<Position> snakePos = m_snake->getPos();
    for (unsigned int i = 1; i < snakePos.size(); i++) {
        if (snakePos[0] == snakePos[i]) m_gameover = true;
    }

    // Apple
    if (snakePos[0] == m_apple) {
        m_score += 10;
        m_snake->growUp();

        snakePos = m_snake->getPos();
        int x, y;
        bool incorrectPos;
        do {
            x = rand() % (m_map->getWidth()-2) + 1;
            y = rand() % (m_map->getHeight()-3) + 2;
            incorrectPos = false;
            for (unsigned int i = 0; i < snakePos.size(); i++) {
                if (x == snakePos[i].x && y == snakePos[i].y) {
                    incorrectPos = true;
                    break;
                }
            }
        } while (incorrectPos);
        m_apple.x = x;
        m_apple.y = y;
    }
}

/***********
 * Sprites *
 ***********/

QPixmap* Game::m_appleSprite;

void Game::loadSprites() {
    Game::m_appleSprite = new QPixmap("../Snake/sprites/apple.png");
}
