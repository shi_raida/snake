#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QTimer>
#include <QMainWindow>
#include "game.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void move();

protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    // Initialisation
    void loadSprites();
    void setSize();
    void setConnections() const;
    void setLabels();

    // Updates
    void gameover();
    void restart();

    // UI
    Ui::MainWindow *ui;
    QLabel* m_gameoverLabel;
    QLabel* m_restartLabel;
    QLabel* m_scoreLabel1;
    QLabel* m_scoreLabel2;

    // Game
    Game* m_game;
    QSize m_tileSize;
    QTimer* m_timer;
};
#endif // MAINWINDOW_H
