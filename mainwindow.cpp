#include <QPainter>
#include <QKeyEvent>
#include "mainwindow.h"
#include "./ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    m_game = new Game();
    loadSprites();
    setSize();
    setLabels();
    m_timer = new QTimer(this);
    setConnections();
    restart();
}

MainWindow::~MainWindow() {
    delete ui;
}

/******************
 * Initialisation *
 ******************/

void MainWindow::loadSprites() {
    Map::loadSprites();
    Snake::loadSprites();
    Game::loadSprites();

    m_tileSize = Map::getWaySprite().size();
}

void MainWindow::setSize() {
    setFixedSize(m_tileSize.width()*m_game->getMap()->getWidth(),
                 m_tileSize.height()*m_game->getMap()->getHeight());
}

void MainWindow::setConnections() const {
    connect(m_timer, SIGNAL(timeout()), this, SLOT(move()));
}

void MainWindow::setLabels() {
    m_gameoverLabel = new QLabel("Game Over", this);
    m_gameoverLabel->setGeometry(77, 267, 900, 126);
    m_gameoverLabel->setFont(QFont("Arial", 126, QFont::Bold));
    m_gameoverLabel->hide();

    m_restartLabel = new QLabel("Press 'R' to restart", this);
    m_restartLabel->setGeometry(270, 500, 900, 50);
    m_restartLabel->setFont(QFont("Arial", 50, -1, true));
    m_restartLabel->hide();

    m_scoreLabel1 = new QLabel(" Score : ", this);
    m_scoreLabel1->setGeometry(65, 15, 250, 31);
    m_scoreLabel1->setFont(QFont("Arial", 25, QFont::Bold, true));
    m_scoreLabel1->setAutoFillBackground(true); // IMPORTANT!
    QPalette pal = m_scoreLabel1->palette();
    pal.setColor(QPalette::Window, QColor(Qt::white));
    m_scoreLabel1->setPalette(pal);

    m_scoreLabel2 = new QLabel("0", this);
    m_scoreLabel2->setGeometry(130, 15, 175, 31);
    m_scoreLabel2->setFont(QFont("Arial", 25, QFont::Bold, true));
    m_scoreLabel2->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
}

/**********
 * Events *
 **********/

void MainWindow::paintEvent(QPaintEvent*) {
    QPainter painter(this);

    // Map
    const Map* map = m_game->getMap();
    for (int i = map->getMinX(); i < map->getMaxX(); i++) {
        for (int j = map->getMinY(); j < map->getMaxY(); j++) {
            switch (map->at(Position(i, j))) {
            case WALL:
                painter.drawPixmap(i*m_tileSize.width(), j*m_tileSize.height(), Map::getWallSprite());
                break;
            case WAY:
                painter.drawPixmap(i*m_tileSize.width(), j*m_tileSize.height(), Map::getWaySprite());
                break;
            }
        }
    }

    // Snake
    const std::vector<Position> snakePos = m_game->getSnake()->getPos();
    const Position snakeDir = m_game->getSnake()->getDir();
    Position delta(0, 0);
    // Head
    if (snakeDir.x == 1) painter.drawPixmap(snakePos[0].x*m_tileSize.width(), snakePos[0].y*m_tileSize.height(), Snake::getHeadRSprite());
    else if (snakeDir.x == -1) painter.drawPixmap(snakePos[0].x*m_tileSize.width(), snakePos[0].y*m_tileSize.height(), Snake::getHeadLSprite());
    else if (snakeDir.y == 1) painter.drawPixmap(snakePos[0].x*m_tileSize.width(), snakePos[0].y*m_tileSize.height(), Snake::getHeadBSprite());
    else if (snakeDir.y == -1) painter.drawPixmap(snakePos[0].x*m_tileSize.width(), snakePos[0].y*m_tileSize.height(), Snake::getHeadTSprite());
    // Body
    for (unsigned int i = 1; i < snakePos.size()-1; i++) {
        bool t(false), r(false), b(false), l(false);
        for (unsigned int j = 0; j < 3; j+=2) {
            delta = snakePos[i] - snakePos[i-1+j];
            if (delta.x == 1) l = true;
            else if (delta.x == -1) r = true;
            else if (delta.y == 1) t = true;
            else if (delta.y == -1) b = true;
        }
        if (t && b) painter.drawPixmap(snakePos[i].x*m_tileSize.width(), snakePos[i].y*m_tileSize.height(), Snake::getBodyVSprite());
        else if (r && l) painter.drawPixmap(snakePos[i].x*m_tileSize.width(), snakePos[i].y*m_tileSize.height(), Snake::getBodyHSprite());
        else if (t && r) painter.drawPixmap(snakePos[i].x*m_tileSize.width(), snakePos[i].y*m_tileSize.height(), Snake::getBodyBLSprite());
        else if (t && l) painter.drawPixmap(snakePos[i].x*m_tileSize.width(), snakePos[i].y*m_tileSize.height(), Snake::getBodyBRSprite());
        else if (b && r) painter.drawPixmap(snakePos[i].x*m_tileSize.width(), snakePos[i].y*m_tileSize.height(), Snake::getBodyTLSprite());
        else if (b && l) painter.drawPixmap(snakePos[i].x*m_tileSize.width(), snakePos[i].y*m_tileSize.height(), Snake::getBodyTRSprite());
    }
    // Queue
    delta = snakePos[snakePos.size()-1] - snakePos[snakePos.size()-2];
    if (delta.x == 1) painter.drawPixmap(snakePos[snakePos.size()-1].x*m_tileSize.width(), snakePos[snakePos.size()-1].y*m_tileSize.height(), Snake::getQueueLSprite());
    else if (delta.x == -1) painter.drawPixmap(snakePos[snakePos.size()-1].x*m_tileSize.width(), snakePos[snakePos.size()-1].y*m_tileSize.height(), Snake::getQueueRSprite());
    else if (delta.y == 1) painter.drawPixmap(snakePos[snakePos.size()-1].x*m_tileSize.width(), snakePos[snakePos.size()-1].y*m_tileSize.height(), Snake::getQueueTSprite());
    else if (delta.y == -1) painter.drawPixmap(snakePos[snakePos.size()-1].x*m_tileSize.width(), snakePos[snakePos.size()-1].y*m_tileSize.height(), Snake::getQueueBSprite());

    // Apple
    Position apple = m_game->getApple();
    painter.drawPixmap(apple.x*m_tileSize.width(), apple.y*m_tileSize.height(), Game::getAppleSprite());
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (!m_game->isGameover()) {
        Position dir(0, 0);
        if (event->key() == Qt::Key_Z)
            dir.y = -1;
        else if (event->key() == Qt::Key_S)
            dir.y = 1;
        else if (event->key() == Qt::Key_D)
            dir.x = 1;
        else if (event->key() == Qt::Key_Q)
            dir.x = -1;
        m_game->getSnake()->setDir(dir);
    } else {
        if (event->key() == Qt::Key_R)
            restart();
    }
}

/***********
 * Updates *
 ***********/

void MainWindow::move() {
    m_game->move();
    m_scoreLabel2->setNum((int)m_game->getScore());
    if (m_game->isGameover())
        gameover();
    update();
}

void MainWindow::gameover() {
    m_timer->stop();
    m_gameoverLabel->show();
    m_restartLabel->show();
}

void MainWindow::restart() {
    m_game = new Game();
    m_gameoverLabel->hide();
    m_restartLabel->hide();
    update();
    m_timer->start(200);
}
